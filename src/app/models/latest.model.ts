export interface Rate {
    [key: string]: number;
}

export class LatestResponse {
  base: string;
  date: string;
  rates: Rate;
  success: boolean;
  timestamp: number;
}
