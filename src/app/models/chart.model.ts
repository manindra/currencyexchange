export class DataSet {
  value: number;
  label: string;
}

export class ChartData {
  chart: any;
  data: DataSet[];
}

export interface RateRecord {
    [key: string]: number;
}

export interface RateHistory {
  [key: string]: RateRecord;
}

export class TimeSeriesResponse {
  base: string;
  symbol: string;
  end_date: string;
  rates: RateHistory;
  start_date: string;
  success: boolean;
  timeseries: boolean;
}
