// export class Symbol {
//   [key: string]: Record<string, string>
// }

export interface SymbolF {
    [key: string]: string;
}

export class SymbolResponse {
  symbols: SymbolF;
  success: boolean;
}
