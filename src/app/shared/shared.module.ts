import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);

import { NavbarComponent } from './components/navbar/navbar.component';
import { ConverterCardComponent } from './components/converter-card/converter-card.component';
import { DetailsChartComponent } from './components/details-chart/details-chart.component';

@NgModule({
  declarations: [
    NavbarComponent,
    ConverterCardComponent,
    DetailsChartComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FusionChartsModule
  ],
  exports: [
    NavbarComponent,
    ConverterCardComponent,
    DetailsChartComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA
    ],
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA
  // ]
})
export class SharedModule { }
