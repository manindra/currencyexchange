import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DetailsChartComponent } from './details-chart.component';
import { FusionChartsModule } from 'angular-fusioncharts';

describe('DetailsChartComponent', () => {
  let component: DetailsChartComponent;
  let fixture: ComponentFixture<DetailsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        FusionChartsModule.forRoot()
      ],
      declarations: [ DetailsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
