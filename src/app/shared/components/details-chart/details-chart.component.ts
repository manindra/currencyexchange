import { Component, OnDestroy , OnInit, Input } from '@angular/core';
import { Observable, BehaviorSubject, Subscription  } from 'rxjs';
import { FixerService } from '@services/index';
import { ChartData, TimeSeriesResponse } from '@models/index';
import { SymbolsDataResponse, LatestDataResponse, TimeSeriesDataResponse } from 'assets/responses';

@Component({
  selector: 'app-details-chart',
  templateUrl: './details-chart.component.html',
  styleUrls: ['./details-chart.component.scss']
})
export class DetailsChartComponent implements OnInit, OnDestroy {

  @Input() from: string;
  @Input() to: string;

  sub: Subscription = new Subscription();

  chartType: string = 'line';
  chartDataFormat: string = 'json';

  chartDataSource$: BehaviorSubject<ChartData> = new BehaviorSubject<ChartData>(null);

  constructor(private fixerService: FixerService) {
  }

  ngOnInit(): void {

    const sub1: Subscription = this.fixerService.chartDataTimeSeries$.subscribe((dataSource: any) => {
      if (dataSource) {
        this.chartDataSource$.next(dataSource);
      } else {
        // this.prepareCharData();
        this.getChartData();
      }
    });
    this.sub.add(sub1);
  }

  prepareCharData(): void {
    this.fixerService.setChartData(TimeSeriesDataResponse);
  }

  getChartData(): void {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const start_date = [ year - 1, ('0' + month).slice(-2), date.getDate() ].join('-');
    const end_date = [ year, ('0' + month).slice(-2), date.getDate() ].join('-');
    const sub1: Subscription = this.fixerService.timeSeries(this.from, [ this.to ], start_date, end_date)
    .subscribe((timeSeriesResponse: TimeSeriesResponse) => {
      timeSeriesResponse.symbol = this.to;
      this.fixerService.setChartData(timeSeriesResponse);
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
