import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { FixerService } from '@services/index';
import { SymbolF, Rate } from '@models/index';

@Component({
  selector: 'app-converter-card',
  templateUrl: './converter-card.component.html',
  styleUrls: ['./converter-card.component.scss']
})
export class ConverterCardComponent implements OnInit, OnDestroy {

  sub: Subscription = new Subscription();

  @Input() from: string = 'EUR';
  @Input() to: string = 'USD';
  @Output() amountChange = new EventEmitter<number>();

  amount: number = 1;
  toRate: number = 1.2314;
  result: number = 100.45;

  symbols: SymbolF;
  rates: Rate;
  allSymbols: string[] = [];
  @Input() isDetails: boolean = false;

  constructor(
    private fixerService: FixerService,
    private router: Router) {
  }

  ngOnInit() {
    const sub1: Subscription = this.fixerService.symbols$.subscribe((symbols: SymbolF) => {
      if (symbols) {
        this.symbols = symbols;
        this.allSymbols = Object.keys(symbols);
      }
    });
    this.sub.add(sub1);

    const sub2: Subscription = this.fixerService.rates$.subscribe((rates: Rate) => {
      this.rates = rates;
    });
    this.sub.add(sub2);
  }

  swap(): void {
    if (this.isDetails) {
      this.router.navigate(['/details'], { queryParams: { from: this.to, to: this.from } });
    } else {
      const t = this.from;
      this.from = this.to;
      this.to = t;
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
