import { Observable } from 'rxjs';
// import { ApiService } from './api.service';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  public baseProjectUrl: string;
  private http: HttpClient;
  constructor(
    http: HttpClient
  ) {
    this.http = http;
    this.baseProjectUrl = environment.baseUrl;
  }

  public get headers() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }

  // regionU level
  public get(relativeUrl: string, httpParams?: HttpParams): Observable<any> {
    const url: string = this.baseProjectUrl + relativeUrl;
    const params: HttpParams = httpParams || new HttpParams();

    const options: object = {
      responseType: 'json',
      ...this.headers,
      params
    };

    return this.http.get(url, options);
  }

  public post(relativeUrl: string, data: any, otherOptions?: any): Observable<any> {
    const url: string = this.baseProjectUrl + relativeUrl;
    const body = JSON.stringify(data);
    const options: object = {
      responseType: 'json',
      ...this.headers,
      ...otherOptions,
    };
    return this.http.post(url, body, options);
  }

  public postFormData(relativeUrl: string, data: FormData, options: any = {}): Observable<any> {
    const url: string = this.baseProjectUrl + relativeUrl;

    return this.http.post(url, data, options);
  }

  public put(relativeUrl: string, data: any, otherOptions?: any): Observable<any> {
    const url: string = this.baseProjectUrl + relativeUrl;
    const body = JSON.stringify(data);
    const options: object = {
      responseType: 'json',
      ...this.headers,
      ...otherOptions,
    };
    return this.http.put(url, body, options);
  }

  public delete(relativeUrl: string): Observable<any> {
    const url: string = this.baseProjectUrl + relativeUrl;
    const options: object = {
      responseType: 'json',
      ...this.headers,
    };
    return this.http.delete(url, options);
  }

}
