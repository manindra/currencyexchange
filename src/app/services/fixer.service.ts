import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { BaseService } from './base.service';

import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { SymbolF, SymbolResponse, LatestResponse, Rate, TimeSeriesResponse, ChartData, DataSet } from '@models/index';

const monthDays: number[] = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
const monthNames: string[] = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

@Injectable({
  providedIn: 'root'
})
export class FixerService extends BaseService {

  public symbolsPreferred: string[] = ['USD', 'EUR', 'GBP', 'THB', 'SGD', 'CAD', 'AED', 'INR', 'CNY', 'JPY'];

  private _symbols: BehaviorSubject<SymbolF> = new BehaviorSubject<SymbolF>(null);
  public symbols$: Observable<SymbolF> = this._symbols.asObservable();

  private _rates: BehaviorSubject<Rate> = new BehaviorSubject<Rate>(null);
  public rates$: Observable<Rate> = this._rates.asObservable();

  private _chartDataTimeSeries: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public chartDataTimeSeries$: Observable<any> = this._chartDataTimeSeries.asObservable();

  constructor(private httpClient: HttpClient) {
    super(httpClient);
  }

  getSymbols(): Observable<SymbolResponse> {
    const relativeUrl = '/symbols';
    return this.get(relativeUrl);
  }

  setSymbols(symbols: SymbolF): void {
    this._symbols.next(symbols);
  }

  convert(to: string, from: string, amount: number): Observable<any> {
    const relativeUrl = '/convert';

    const params: HttpParams = new HttpParams()
    .set('to', to)
    .set('from', from)
    .set('amount', '' + amount);

    return this.get(relativeUrl, params);
  }

  latest(
      base: string,
      symbols: string[] = ['USD', 'EUR', 'GBP', 'THB', 'SGD', 'CAD', 'AED', 'INR', 'CNY', 'JPY']
    ): Observable<LatestResponse> {
    const relativeUrl = '/latest';

    const params: HttpParams = new HttpParams()
    .set('base', base)
    .set('symbols', symbols.join(','));

    return this.get(relativeUrl, params);
  }

  setRates(rates: Rate): void {
    this._rates.next(rates);
  }

  historicalData(base: string, symbols: string[], amount: number, date: string): Observable<any> {
    const relativeUrl = '/convert';

    const params: HttpParams = new HttpParams()
    .set('base', base)
    .set('symbols', symbols.join(','))
    .set('amount', '' + amount)
    .set('date', date);

    return this.get(relativeUrl, params);
  }

  // date yyyy-mm-dd
  timeSeries(base: string, symbols: string[], start_date: string, end_date: string): Observable<TimeSeriesResponse> {
    const relativeUrl = '/timeseries';

    const params: HttpParams = new HttpParams()
    .set('base', base)
    .set('symbols', symbols.join(','))
    .set('start_date', start_date)
    .set('end_date', end_date);

    return this.get(relativeUrl, params);
  }

  setChartData(timeSeriesResponse: TimeSeriesResponse): void {
    const chartData: ChartData = {
      chart: {
        // Set the chart caption
        caption: 'Past year Historical monthly data',
        // Set the chart subcaption
        subCaption: `${timeSeriesResponse.base} vs ${timeSeriesResponse.symbol}`,
        // Set the x-axis name
        xAxisName: 'Months',
        // Set the y-axis name
        yAxisName: timeSeriesResponse.symbol,
        numberSuffix: timeSeriesResponse.symbol,
        // Set the theme for your chart
        theme: 'fusion'
      },
      // Chart Data - from step 2
      data: this.getChartData(timeSeriesResponse)
    };
    this._chartDataTimeSeries.next(chartData);
  }

  getChartData(data: TimeSeriesResponse): DataSet[] {
    const date: Date = new Date();
    date.setMonth(parseInt(data.end_date.split('-')[1], 10) - 1);

    let currMonth: number = date.getMonth();
    let year: number = date.getFullYear();
    if (year % 4 === 0 ) {
      monthDays[1] = 29;
    } else {
      monthDays[1] = 28;
    }

    const dates: { label: string, month: number }[] = [ { label: data.end_date, month: currMonth } ];
    currMonth--;

    for ( let i = 0; i < 11; i++) {
        if ( currMonth > -1 ) {
            dates.push({ month: currMonth, label:  [year, ('0' + (currMonth + 1)).slice(-2), monthDays[currMonth]].join('-') });
            currMonth--;
        }
        if (currMonth === -1) {
          year--;
          currMonth = 11;
        }
    }
    const dataSets: DataSet[] = [];
    // console.log(dates);
    dates.reverse().forEach(({label, month}: {label: string, month: number}) => {
      if (data.symbol in data.rates[label]) {
        dataSets.push({
          label: monthNames[month], value: data.rates[label][data.symbol]
        });
      }
    });

    // Object.keys(data.rates).forEach(label => chartData.push({label, value: data.rates[label][data.symbol]}));
    return dataSets;
  }

}
