import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FixerService } from './fixer.service';
import { DataSet } from '@models/index';
import { TimeSeriesDataResponse  } from 'assets/responses';

describe('FixerService', () => {
  let service: FixerService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(FixerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('shoudd retur chartdata', () => {
    const chartData: DataSet[] = service.getChartData(TimeSeriesDataResponse);
    expect(chartData.length).toBe(12);
  });


});
