import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { SharedModule } from '@shared/shared.module';
import { FixerService } from '@services/index';
import { HomeComponent } from './home.component';
import { SymbolsDataResponse, LatestDataResponse } from 'assets/responses';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        RouterTestingModule
      ],
      declarations: [ HomeComponent ]
    })
    .compileComponents();
    spyOn(FixerService.prototype, 'getSymbols').and.returnValue(of(SymbolsDataResponse));
    spyOn(FixerService.prototype, 'latest').and.returnValue(of(LatestDataResponse));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should called getSymbols', () => {
    component.getSymbols();
    expect(FixerService.prototype.getSymbols).toHaveBeenCalled();
    expect(component.symbols).not.toBe({});
  });

  it('should called getRates', () => {
    // component.getRates();
    expect(FixerService.prototype.latest).toHaveBeenCalled();
    expect(FixerService.prototype.latest).toHaveBeenCalledTimes(1);
    expect(component.rates).not.toBe({});
  });

  it('should update amount onAmountUpdate', () => {
    component.onAmountUpdate(10);
    expect(component.amount).toBe(10);
  });

});
