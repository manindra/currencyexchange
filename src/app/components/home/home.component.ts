import { Component, OnInit, OnDestroy } from '@angular/core';
import { FixerService } from '@services/index';
import { SymbolResponse, SymbolF, LatestResponse, Rate } from '@models/index';
import { SymbolsDataResponse, LatestDataResponse } from 'assets/responses';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  sub: Subscription = new Subscription();
  symbols: SymbolF;
  rates: Rate;
  preferedSymbols: string[] = ['AED'];
  from: string = 'EUR';
  amount: number = 1;

  constructor(private fixerService: FixerService) {
    this.preferedSymbols = this.fixerService.symbolsPreferred;
  }

  ngOnInit() {
    const sub1: Subscription = this.fixerService.symbols$.subscribe((symbols: SymbolF) => {
      if (symbols) {
         this.symbols = symbols;
      } else {
        this.getSymbols();
      }
    });
    this.sub.add(sub1);

    const sub2: Subscription = this.fixerService.rates$.subscribe((rates: Rate) => {
      if (rates) {
        this.rates = rates;
      } else {
        this.getRates();
      }
    });
    this.sub.add(sub2);

  }

  getSymbols(): void {
    // this.fixerService.setSymbols(SymbolsDataResponse.symbols);
    const sub3: Subscription = this.fixerService.getSymbols().subscribe((res: SymbolResponse) => {
      this.fixerService.setSymbols(res.symbols);
    });
    this.sub.add(sub3);
  }

  getRates(): void {
    // this.fixerService.setRates(LatestDataResponse.rates);
    const sub4: Subscription = this.fixerService.latest(this.from).subscribe((latest: LatestResponse) => {
      this.fixerService.setRates(latest.rates);
    });
    this.sub.add(sub4);
  }

  onAmountUpdate(event: number): void {
    this.amount = event;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
