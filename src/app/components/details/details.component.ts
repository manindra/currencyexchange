import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { FixerService } from '@services/index';
import { SymbolF, SymbolResponse } from '@models/index';
import { SymbolsDataResponse } from 'assets/responses';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {

  sub: Subscription = new Subscription();
  from: string;
  to: string;
  fromName: string;
  isDetails = true;

  constructor(private route: ActivatedRoute, private fixerService: FixerService) { }

  ngOnInit() {
    const subParams: Subscription = this.route.queryParams.subscribe(params => {
      this.from = params.from;
      this.to = params.to;
    });
    this.sub.add(subParams);

    const sub1: Subscription = this.fixerService.symbols$.subscribe((symbols: SymbolF) => {
      if (symbols) {
        this.fromName = symbols[this.from];
      } else {
        this.getSymbols();
      }
    });
    this.sub.add(sub1);
  }


  getSymbols(): void {
    // this.fixerService.setSymbols(SymbolsDataResponse.symbols);
    const sub3: Subscription = this.fixerService.getSymbols().subscribe((res: SymbolResponse) => {
      this.fixerService.setSymbols(res.symbols);
    });
    this.sub.add(sub3);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
