import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@shared/shared.module';
import { DetailsComponent } from './details.component';

import { of } from 'rxjs';
import { FixerService } from '@services/index';
import { SymbolsDataResponse } from 'assets/responses';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let service: FixerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ DetailsComponent ]
    })
    .compileComponents();
    spyOn(FixerService.prototype, 'getSymbols').and.returnValue(of(SymbolsDataResponse));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get symbols using getSymbols', () => {
    expect(FixerService.prototype.getSymbols).toHaveBeenCalled();
  });
});
